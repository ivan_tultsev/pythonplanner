from trees.tree_node import TreeNode
from trees.tree_node_scored import TreeNodeScored
from planner.action import Action
from planner.action_planner import Planner

discover_vms = Action(
    "discover VMs",
    {"Region","RG"},
    {"VM"})
discover_dbs = Action(
    "discover DBs",
    {"Region"},
    {"DB"})
discover_regions = Action(
    "discover Regions",
    {},
    {"Region"})
discover_rgs = Action(
    "discover Resource Groups",
    {"Region"},
    {"RG"})
planner = Planner({
    discover_dbs,
    discover_regions,
    discover_vms, 
    discover_rgs})
target_effects = {"DB", "VM"}
last_step = planner.make_plan(target_effects)
print("The Plan:\nGoal " + str(target_effects))
last_step.get_root().print_tree(last_step)