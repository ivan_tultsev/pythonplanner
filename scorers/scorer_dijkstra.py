from scorers.scorer_const import *

class ScorerDijkstra(ScorerConst):
    def get_score(self):
        result = self.scorable.score
        if self.scorable.parent is not None:
            result += self.scorable.parent.get_score()
        return result