class TreeNode:
    def __init__(self, content, parent = None):
        self.parent = parent
        self.content = content
        self.children = []

    def add_child(self, child):
        self.children.append(child)
        child.parent = self

    def get_path(self):
        result = []
        if self.parent is not None:
            result.append(self.parent)
            result.extend(self.parent.get_path())
        return result

    def get_root(self):
        return self.get_path().pop()

    def get_leaf_nodes(self):
        result = []
        if len(self.children) == 0:
            result.append(self)
        else:
            for child in self.children:
                result.extend(child.get_leaf_nodes())
        return result

    def print_tree(self, node_to_mark = None, prefix = ""):
        i = 0
        no_of_spaces = 4
        for child in self.children:
            i += 1
            print(
                prefix 
                + "|" 
                + "_" * no_of_spaces 
                + str(child.content) 
                + (" +" if child == node_to_mark else "")
            )
            child.print_tree(
                node_to_mark,
                prefix + ("|" if i < len(self.children) else " ") + " "*no_of_spaces
            )