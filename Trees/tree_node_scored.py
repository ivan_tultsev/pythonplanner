from trees.tree_node import TreeNode
from scorers.scorer_dijkstra import ScorerDijkstra

class TreeNodeScored(TreeNode):
    def __init__(self, content, score = 0, parent = None):
        super().__init__(content, parent)
        self.score = score
        self.scorer = ScorerDijkstra(self)

    def get_score(self):
        return self.scorer.get_score()
