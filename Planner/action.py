import collections

class Action:
    def __init__(self, action, conditions = {}, effects = {}):
        self.action = action
        self.effects = effects
        self.conditions = conditions

    def resolve_conditions(self, action):
        self.conditions = self.conditions.union(action.conditions)
        self.conditions.difference_update(self.effects)
        return self

    def resolves_any_condition(self, conditions = {}):
        return len(self.effects.intersection(conditions)) > 0

    def copy(self):
        return Action(
            self.action,
            set(self.conditions),
            set(self.effects)
        )

    def __str__(self):
        return str(self.action) + " " + str(self.conditions)
