from trees.tree_node_scored import TreeNodeScored
from planner.action import Action
import copy

class Planner:
    def __init__(self, actions = {}):
        self.actions = actions

    def make_plan(self, end_state_conditions: set = set()):
        goal_action = Action("Goal", end_state_conditions)
        root_node = TreeNodeScored(goal_action)
        node_to_expand = root_node

        while len(node_to_expand.content.conditions) > 0:
            self.expand_node(node_to_expand)
            node_to_expand = self.get_optimal_node(root_node)

        return node_to_expand

    def expand_node(self, node: TreeNodeScored):
        for action in self.actions:
            if action.resolves_any_condition(node.content.conditions):
                node.add_child(
                    TreeNodeScored(
                        action.copy().resolve_conditions(node.content), 
                        1, 
                        node
                    )
                )

    def get_optimal_node(self, root_node: TreeNodeScored):
        result = None
        for node in root_node.get_leaf_nodes():
            if result is None or node.get_score() < result.get_score():
                result = node
        return result